# Changelog

## Breaking changes

A series of commits ending in commit
6d20f3a928697d5b4bcb507b60b197407dc9a70a on April 22, 2021 introduced
breaking changes and some additions to the VMOD:

- Unrecoverable errors now lead to VCL failure. Previously, functions
  and methods returned sentinel values under error conditions (such as
  NULL for string returns) and continued processing. This could lead
  to fatal errors going unnoticed. VCL failure was already used for
  the set integer() method, since there is no sentinel value for
  integers. See the ERRORS section of README and the message in commit
  c70e410a8a152453bd428278dca9ad9045bf4ebd for details.

- Added the methods `.subroutine()` and `.check_call()`.

The changed error handling is a breaking change. Typically this may
nevertheless require few or no changes in production VCL, since these
are "can't happen" errors that are ordinarily discovered in
development and testing, and don't make it into production (or else
they are noticed quickly). But uses of the new version should be
checked nonetheless.

In detail:

### Changed

- Unrecoverable errors now lead to VCL failure. See the documentation
  for details, in particular the ERRORS section of the manual
  (README).

- Set `.compile()` is now unnecessary and deprecated. Set compile
  is now done automatically when `vcl_init` completes.

### Added

- Set methods `.subroutine()` and `.check_call()`
