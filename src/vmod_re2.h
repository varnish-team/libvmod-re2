/*-
 * Copyright (c) 2017 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/* for strdup() */
#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>

#include "cache/cache.h"
#include "vcl.h"
#include "vsb.h"

#include "vcc_if.h"
#include "vre2/vre2.h"

#define VFAIL0(ctx, msg) VRT_fail((ctx), "vmod re2 failure: " msg)

#define VFAIL(ctx, fmt, ...) \
	VRT_fail((ctx), "vmod re2 failure: " fmt, __VA_ARGS__)

#define VERRNOMEM(ctx, fmt, ...) \
	VFAIL((ctx), fmt ", out of space", __VA_ARGS__)

#define VNOTICE(ctx, fmt, ...) \
	VSLb((ctx)->vsl, SLT_Notice, "vmod_re2: " fmt, __VA_ARGS__)

struct vmod_re2_regex {
	unsigned	magic;
#define VMOD_RE2_REGEX_MAGIC 0x5c3f6f24
	vre2		*vre2;
	char		*vcl_name;
	int		ngroups;
	unsigned	never_capture;
};

/* Defined in vmod_re2.c */
extern const char * const rewrite_name[];
