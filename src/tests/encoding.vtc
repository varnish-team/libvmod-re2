# looks like -*- vcl -*-

varnishtest "latin1 and utf8 encoding"

# Tests from re2 testing/re2_test.cc

# regex object and match function

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
	       new ninebytes = re2.regex("^.........$");
	       new threechars = re2.regex("^...$", utf8=true);
	       new capture_one_byte = re2.regex("(.)");
	       new capture_one_char = re2.regex("(.)", utf8=true);
	       new bytes = re2.regex({"^日扼語$"});
	       new chars = re2.regex({"^日扼語$"}, utf8=true);
	       new patternbytes = re2.regex({"^.扼.$"});
	       new patternchars = re2.regex({"^.扼.$"}, utf8=true);
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		if (ninebytes.match({"日扼語"})) {
			set resp.http.ninebytes = "match";
		}
		if (threechars.match({"日扼語"})) {
			set resp.http.threechars = "match";
		}
		if (capture_one_byte.match({"日扼語"})) {
			if (capture_one_byte.backref(1) == {"�"}) {
				set resp.http.captured_one_byte = "true";
			}
		}
		if (capture_one_char.match({"日扼語"})) {
			if (capture_one_char.backref(1) == {"日"}) {
				set resp.http.captured_one_char = "true";
			}
		}
		if (bytes.match({"日扼語"})) {
			set resp.http.bytes = "match";
		}
		if (chars.match({"日扼語"})) {
			set resp.http.chars = "match";
		}
		if (patternbytes.match({"日扼語"})) {
			set resp.http.patternbytes = "match";
		}
		if (patternchars.match({"日扼語"})) {
			set resp.http.patternchars = "match";
		}

		# Function interface
		if (re2.match("^.........$",
		              {"日扼語"})) {
			set resp.http.ninebytesf = "match";
		}
		if (re2.match("^...$",
		              {"日扼語"},
		              utf8=true)) {
			set resp.http.threecharsf = "match";
		}
		if (re2.match("(.)",{"日扼語"})) {
			if (re2.backref(1) == {"�"}) {
				set resp.http.captured_one_bytef = "true";
			}
		}
		if (re2.match("(.)",
		              {"日扼語"},
		              utf8=true)) {
			if (re2.backref(1) == {"日"}) {
				set resp.http.captured_one_charf = "true";
			}
		}
		if (re2.match({"日扼語"},
		              {"日扼語"})) {
			set resp.http.bytesf = "match";
		}
		if (re2.match({"日扼語"},
		              {"日扼語"},
		              utf8=true)) {
			set resp.http.charsf = "match";
		}
		if (re2.match({"^.扼.$"},
		              {"日扼語"})) {
			set resp.http.patternbytesf = "match";
		}
		if (re2.match({"^.扼.$"},
		              {"日扼語"},
		              utf8=true)) {
			set resp.http.patterncharsf = "match";
		}
	}

} -start

client c1 {
	txreq
	rxresp
	expect resp.http.ninebytes == "match"
	expect resp.http.threechars == "match"
	expect resp.http.captured_one_byte == "true"
	expect resp.http.captured_one_char == "true"
	expect resp.http.bytes == "match"
	expect resp.http.chars == "match"
	expect resp.http.patternbytes == <undef>
	expect resp.http.patternchars == "match"

	expect resp.http.ninebytesf == "match"
	expect resp.http.threecharsf == "match"
	expect resp.http.captured_one_bytef == "true"
	expect resp.http.captured_one_charf == "true"
	expect resp.http.bytesf == "match"
	expect resp.http.charsf == "match"
	expect resp.http.patternbytesf == <undef>
	expect resp.http.patterncharsf == "match"
} -run

# set object

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_init {
	       new ninebytes = re2.set();
	       ninebytes.add("^.........$");

	       new threechars = re2.set(utf8=true);
	       threechars.add("^...$");

	       new bytes = re2.set();
	       bytes.add({"^日扼語$"});

	       new chars = re2.set(utf8=true);
	       chars.add({"^日扼語$"});

	       new patternbytes = re2.set();
	       patternbytes.add({"^.扼.$"});

	       new patternchars = re2.set(utf8=true);
	       patternchars.add({"^.扼.$"});
	}

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		if (ninebytes.match({"日扼語"})) {
			set resp.http.ninebytes = "match";
		}
		if (threechars.match({"日扼語"})) {
			set resp.http.threechars = "match";
		}
		if (bytes.match({"日扼語"})) {
			set resp.http.bytes = "match";
		}
		if (chars.match({"日扼語"})) {
			set resp.http.chars = "match";
		}
		if (patternbytes.match({"日扼語"})) {
			set resp.http.patternbytes = "match";
		}
		if (patternchars.match({"日扼語"})) {
			set resp.http.patternchars = "match";
		}
	}

}

client c1 {
	txreq
	rxresp
	expect resp.http.ninebytes == "match"
	expect resp.http.threechars == "match"
	expect resp.http.bytes == "match"
	expect resp.http.chars == "match"
	expect resp.http.patternbytes == <undef>
	expect resp.http.patternchars == "match"
} -run

# sub() function

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.ninebytes =
		  re2.sub("^.........$",
		          {"日扼語"}, "z");
		set resp.http.threechars =
		  re2.sub("^...$",
		          {"日扼語"}, "z",
		          utf8=true);
		set resp.http.bytes =
		  re2.sub({"日扼語"},
		          {"日扼語"}, "z");
		set resp.http.chars =
		  re2.sub({"日扼語"},
		          {"日扼語"}, "z",
			  utf8=true);
		set resp.http.patternbytes =
		  re2.sub({"^.扼.$"},
		          {"日扼語"}, "z",
			  fallback="fallback");
		set resp.http.patternchars =
		  re2.sub({"^.扼.$"},
		          {"日扼語"}, "z",
		          utf8=true);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.http.ninebytes == "z"
	expect resp.http.threechars == "z"
	expect resp.http.bytes == "z"
	expect resp.http.chars == "z"
	expect resp.http.patternbytes == "fallback"
	expect resp.http.patternchars == "z"
} -run

# suball() function

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.ninebytes =
		  re2.suball("^.........$",
		             {"日扼語"}, "z");
		set resp.http.threechars =
		  re2.suball("^...$",
		             {"日扼語"}, "z",
		             utf8=true);
		set resp.http.bytes =
		  re2.suball({"日扼語"},
		             {"日扼語"}, "z");
		set resp.http.chars =
		  re2.suball({"日扼語"},
		             {"日扼語"}, "z",
			     utf8=true);
		set resp.http.patternbytes =
		  re2.suball({"^.扼.$"},
		             {"日扼語"}, "z",
			     fallback="fallback");
		set resp.http.patternchars =
		  re2.suball({"^.扼.$"},
		             {"日扼語"}, "z",
		             utf8=true);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.http.ninebytes == "z"
	expect resp.http.threechars == "z"
	expect resp.http.bytes == "z"
	expect resp.http.chars == "z"
	expect resp.http.patternbytes == "fallback"
	expect resp.http.patternchars == "z"
} -run

# extract() function

varnish v1 -vcl {
	import ${vmod_re2};
	backend b None;

	sub vcl_recv {
		return(synth(200));
	}

	sub vcl_synth {
		set resp.http.ninebytes =
		  re2.extract("^.........$",
		              {"日扼語"}, "z");
		set resp.http.threechars =
		  re2.extract("^...$",
		              {"日扼語"}, "z",
		              utf8=true);
		set resp.http.bytes =
		  re2.extract({"日扼語"},
		              {"日扼語"}, "z");
		set resp.http.chars =
		  re2.extract({"日扼語"},
		              {"日扼語"}, "z",
			      utf8=true);
		set resp.http.patternbytes =
		  re2.extract({"^.扼.$"},
		              {"日扼語"}, "z",
			      fallback="fallback");
		set resp.http.patternchars =
		  re2.extract({"^.扼.$"},
		              {"日扼語"}, "z",
		              utf8=true);
	}
}

client c1 {
	txreq
	rxresp
	expect resp.http.ninebytes == "z"
	expect resp.http.threechars == "z"
	expect resp.http.bytes == "z"
	expect resp.http.chars == "z"
	expect resp.http.patternbytes == "fallback"
	expect resp.http.patternchars == "z"
} -run
